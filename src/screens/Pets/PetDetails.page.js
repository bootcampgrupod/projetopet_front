import { Row, Col, Divider } from "antd";
import { withRouter } from "react-router";
import CarouselComponent from "../../components/Carousel/Carousel.component";
import "./newpet.css";

const PetDetailsPage = (props) => {
  const petId = props.match.params.id;
  console.log(props, "props");
  return (
    // <div className="container">
    <div className=" pet-form">
      <section className="section_form_pet">
        {/* <section name="new_pet" className="pet-form"></section> */}
        <CarouselComponent />
        <Row className="row">
          <Col span={12} className="row">
            <p className="label">Data do acontecimento:</p>
            <p className="value"> 20/02/2020</p>
          </Col>
          <Col span={12} className="row right">
            <p className="label">Data da última atualização:</p>
            <p className="value"> 20/02/2020</p>
          </Col>
        </Row>

        <Divider>Dados do Pet</Divider>
        <Row className="row">
          <Col span={8} className="row">
            <p className="label">Nome do Pet:</p>
            <p className="value"> Marley</p>
          </Col>
          <Col span={8} className="row">
            <p className="label">Idade aproximada:</p>
            <p className="value"> Não informada</p>
          </Col>
          <Col span={8} className="row right">
            <p className="label">Sexo:</p>
            <p className="value"> Não informado</p>
          </Col>
        </Row>

        <Row className="row">
          <Col span={8} className="row">
            <p className="label">Espécie:</p>
            <p className="value"> Marley</p>
          </Col>
          <Col span={8} className="row">
            <p className="label">Raça:</p>
            <p className="value"> Não informada</p>
          </Col>
          <Col span={8} className="row right">
            <p className="label">Cor:</p>
            <p className="value"> Não informado</p>
          </Col>
        </Row>
        <Row className="row">
          <Col span={8} className="row">
            <p className="label">Tamanho:</p>
            <p className="value"> Marley</p>
          </Col>
          <Col span={8} className="row ">
            <p className="label">Castrado:</p>
            <p className="value"> Não informado</p>
          </Col>
        </Row>

        <Divider>Detalhes</Divider>
        <p className="value left"> Não informado</p>

        <Divider>Informações</Divider>
        <Row className="row">
          <Col span={8} className="row">
            <p className="label">Telefone para contato:</p>
            <p className="value"> Marley</p>
          </Col>
          <Col span={8} className="row">
            <p className="label">Endereço:</p>
            <p className="value"> Marley</p>
          </Col>
          <Col span={8} className="row right">
            <p className="label">Número:</p>
            <p className="value"> 253</p>
          </Col>
        </Row>
        <Row className="row">
          <Col span={8} className="row">
            <p className="label">Bairro:</p>
            <p className="value"> Não informado</p>
          </Col>
          <Col span={8} className="row">
            <p className="label">Cidade:</p>
            <p className="value"> Não informado</p>
          </Col>
          <Col span={8} className="row right">
            <p className="label">Estado:</p>
            <p className="value"> Não informado</p>
          </Col>
        </Row>

        {/* "name": "string",
    "type": "string",
    "breed": "string",
    "dateBirth": "2022-01-19T12:28:06.664Z",
    "size": "string",
    "color": "string",
    "castrated": true,
    "createdAt": "2022-01-19T12:28:06.664Z",
  },
  "userId": 0,
  "postAddress": {
    "addressId": 0,
    "address": "string",
    "number": "string",
    "neighborhood": "string",
    "city": "string",
    "state": "string",
    "postalCode": "string",
    "country": "string"
  },
  "status": "string",
  "desc": "string",
  "phone": "string", */}
      </section>
      {/* </div> */}
    </div>
  );
};
export default withRouter(PetDetailsPage);
