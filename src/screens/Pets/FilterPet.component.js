import { Row, Select, Col } from "antd";
import { states } from "./states";
import "./newpet.css";

const { Option } = Select;

const FilterPet = () => {
  return (
    <section className="filter">
      <Row style={{ width: "100%" }}>
        <Col span={6}>
          <Select
            className="input_form"
            placeholder="Espécie"
            allowClear={true}
          >
            <Option value="cachorro" key="cachorro">
              Cachorro
            </Option>
            <Option value="gato" key="gato">
              Gato
            </Option>
            <Option value="outro" key="outro">
              Outro
            </Option>
          </Select>
        </Col>
        <Col span={6}>
          <Select className="input_form" placeholder="Sexo ">
            <Option value="fêmea" key="fêmea">
              Fêmea
            </Option>
            <Option value="macho" key="macho">
              Macho
            </Option>
            <Option value="nao" key="nao">
              Não Informado
            </Option>
          </Select>
        </Col>
        <Col span={6}>
          <Select className="input_form" placeholder="Estado">
            {states.map((item) => (
              <Option value={item.label} key={item.value}>
                {item.value}
              </Option>
            ))}
          </Select>
        </Col>
      </Row>
    </section>
  );
};
export default FilterPet;
