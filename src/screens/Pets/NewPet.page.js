import { withRouter, useHistory } from "react-router-dom";
import { Form, Input, Button, Select, Row, Col, DatePicker } from "antd";
import foto from "../../assets/caes-e-gatos.jpg";
import InputMask from "react-input-mask";
import { createUser } from "../../api/user";
import {
  success,
  error,
  warning,
} from "../../components/Message/Message.component";
import { postPet } from "../../api/pets";
import { states } from "./states";
import "./newpet.css";
import UploadComponent from "../../components/Upload/Upload.component";

const { Option } = Select;
const { TextArea } = Input;

const NewPetPage = (props) => {
  const history = useHistory();
  // const status = props.match.params.id;
  const { status, statusId } = props;
  console.log(props);

  const portes = ["Pequeno", "Médio", "Grande", "Gigante"];
  const onFinish = async (values) => {
    console.log(values, "val");
    const pet = {
      name: values.name || "Desconhecido",
      type: values.type,
      breed: values.breed || "SRD",
      dateBirth: values.dateBirth || "0000-00-00",
      dateOccurrence: values.dateOccurrence,
      size: values.size,
      color: values.color,
      castrated: values.castrated,
      status: statusId,
      desc: values.desc,
      userId: localStorage.getItem("userId"),
      phone: values.phone.replace(/\D/g, ""),
      desc: values.desc,
      address: {
        address: values.address,
        number: values.number,
        neighborhood: values.neighborhood,
        city: values.city,
        state: values.state,
        postalCode: values.postalCode,
      },
    };
    postPet(pet)
      .then((res) => {
        success("Ocorrência criada com sucesso!");
        // history.push('/login')
      })
      .catch((err) => {
        if (err.response.status === 422) {
          warning("");
        } else {
          error("Ocorreu um erro, tente novamente!");
        }
      });
  };

  return (
    <div className="container">
      <div className="container-form-newpet">
        <Form
          name="new_pet"
          className="pet-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <section className="section_form_pet">
            <h1 className="capitalized"> Cadastre o Pet {status}</h1>

            <UploadComponent />
            <Row gutter={{ xs: 8, sm: 8 }}>
              <Col span={12}>
                <Form.Item
                  name="name"
                  rules={[
                    {
                      required: false,
                      message: "Por favor digite o nome do pet",
                    },
                  ]}
                >
                  <Input
                    className="input_form_newpet"
                    type="text"
                    placeholder="Digite o Nome do Pet"
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="dateOccurrence"
                  rules={[
                    {
                      required: true,
                      message:
                        "Por favor digite a data que o pet foi perdido/encontrado",
                    },
                  ]}
                >
                  <DatePicker
                    className="input_form_newpet half"
                    placeholder="Data que o pet foi perdido/encontrado"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Form.Item
                  name="desc"
                  rules={[
                    {
                      required: false,
                      message: "Por favor dê detalhes do acontecimento",
                    },
                  ]}
                >
                  <TextArea
                    className="input_form_newpet"
                    placeholder="Por favor dê detalhes do acontecimento"
                    rows={4}
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16 }}>
              <Col span={8}>
                <Form.Item
                  name="dateBirth"
                  rules={[
                    {
                      required: false,
                      message: "Por favor digite a data de nascimento",
                    },
                  ]}
                >
                  <DatePicker
                    className="input_form_newpet half"
                    placeholder="Data de nascimento"
                  />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  name="size"
                  rules={[
                    {
                      required: true,
                      message: "Por favor escolha o porte",
                    },
                  ]}
                >
                  <Select
                    style={{ width: 120 }}
                    className="input_form_newpet"
                    placeholder="Selecione o porte"
                  >
                    {portes.map((item) => (
                      <Option value={item} key={item}>
                        {item}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  name="castrated"
                  rules={[
                    {
                      required: true,
                      message: "Por favor informe se o animal é cadastrado.",
                    },
                  ]}
                >
                  <Select
                    style={{ width: 120 }}
                    className="input_form_newpet half"
                    placeholder="O pet é castrado? "
                  >
                    <Option value="sim" key="sim">
                      Sim
                    </Option>
                    <Option value="nao" key="nao">
                      Não
                    </Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16 }}>
              <Col span={8}>
                <Form.Item
                  name="type"
                  rules={[
                    {
                      required: true,
                      message:
                        "Por favor digite a espécie (gato, cachorro, etc)",
                    },
                  ]}
                >
                  <Select
                    style={{ width: 120 }}
                    className="input_form_newpet half"
                    placeholder="Espécie (gato, cachorro, etc) * "
                  >
                    <Option value="gato" key="gato">
                      Gato
                    </Option>
                    <Option value="cachorro" key="cachorro">
                      Cachorro
                    </Option>
                    <Option value="outro" key="outro">
                      Outro
                    </Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  name="breed"
                  rules={[
                    {
                      required: false,
                      message: "Por favor digite a raça",
                    },
                  ]}
                >
                  <Input
                    className="input_form_newpet input_half"
                    type="text"
                    placeholder="Raça"
                  />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  name="color"
                  rules={[
                    {
                      required: true,
                      message: "Por favor digite a cor",
                    },
                  ]}
                >
                  <Input
                    className="input_form_newpet half"
                    type="text"
                    placeholder="Digite a cor*"
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16 }}>
              <Col span={18}>
                <Form.Item
                  name="address"
                  rules={[
                    {
                      required: true,
                      message:
                        "Por favor digite seu endereço ou de onde o animal se encontra",
                    },
                  ]}
                >
                  <Input
                    className="input_form_newpet"
                    type="text"
                    placeholder="Endereço*"
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  name="number"
                  rules={[
                    {
                      required: true,
                      message: "Por favor digite o número da casa",
                    },
                  ]}
                >
                  <Input
                    className="input_form_newpet"
                    type="text"
                    placeholder="Número*"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16 }}>
              <Col span={12}>
                <Form.Item
                  name="neighborhood"
                  rules={[
                    {
                      required: true,
                      message: "Por favor digite o bairro",
                    },
                  ]}
                >
                  <Input
                    className="input_form_newpet"
                    type="text"
                    placeholder="Digite o bairro*"
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="city"
                  rules={[
                    {
                      required: true,
                      message: "Por favor digite a cidade",
                    },
                  ]}
                >
                  <Input
                    className="input_form_newpet"
                    type="text"
                    placeholder="Digite a Cidade"
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={{ xs: 8, sm: 16 }}>
              <Col span={8}>
                <Form.Item
                  name="state"
                  rules={[
                    {
                      required: true,
                      message: "Por favor digite o Estado",
                    },
                  ]}
                >
                  <Select
                    style={{ width: 120 }}
                    className="input_form_newpet"
                    placeholder="Selecione o Estado"
                  >
                    {states.map((item) => (
                      <Option value={item.label} key={item.value}>
                        {item.label}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  name="postalCode"
                  rules={[
                    {
                      required: false,
                      message: "Por favor digite o CEP",
                    },
                  ]}
                >
                  <Input
                    className="input_form_newpet"
                    type="text"
                    placeholder="Digite o Cep"
                  />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  name="phone"
                  rules={[
                    {
                      required: true,
                      message: "Por favor digite seu celular",
                    },
                  ]}
                >
                  <InputMask
                    mask="( 99 ) 99999-9999"
                    className="input_form_newpet"
                    placeholder="Celular"
                  />
                </Form.Item>
              </Col>
            </Row>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button button-login-form"
              >
                Enviar
              </Button>
            </Form.Item>
          </section>
        </Form>
      </div>
    </div>
  );
};

export default withRouter(NewPetPage);
