import CardPet from "../../components/CardPet/CardPet";
import "./newpet.css";
import { Pagination } from "antd";
import FilterPet from "./FilterPet.component";

const ListPets = () => {
  return (
    <div>
      <div className="container-form-listpets">
        <FilterPet />
        <section name="new_pet" className="section_pet_card">
          <div className="pet_card">
            <CardPet />
          </div>
          <div className="pet_card">
            <CardPet />
          </div>
          <div className="pet_card">
            <CardPet />
          </div>
          <div className="pet_card">
            <CardPet />
          </div>
          <div className="pet_card">
            <CardPet />
          </div>
          <div className="pet_card">
            <CardPet />
          </div>
          <div className="pet_card">
            <CardPet />
          </div>

          <Pagination defaultCurrent={6} total={500} />
        </section>
      </div>
    </div>
  );
};
export default ListPets;
