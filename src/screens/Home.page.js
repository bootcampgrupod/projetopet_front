import React, { useEffect } from "react";
import "./login.css";
import banner from "../assets/banner_home.png";
import { getPetStatus } from "../api/pets";

const HomePage = () => {
  useEffect(()=> {
    getPetStatus().then(() => console.log(''))
  })
  return (
    <>
      <div className="container_home">
        <img src={banner} className="banner_home" alt="banner" />
        <div className="loginForm">
          <h1> Home em construção</h1>
        </div>
      </div>
    </>
  );
};

export default HomePage;
