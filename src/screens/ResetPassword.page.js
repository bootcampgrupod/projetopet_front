import React from "react";
import "./login.css";
import ResetPassword from "../components/ForgotForm/ResetPassword";

const ResetPasswordPage = () => {
  return (
    <div className="login">
      <div className="loginForm">
        <ResetPassword />
      </div>
    </div>
  );
};

export default ResetPasswordPage;
