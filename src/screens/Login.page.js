import React from "react";
import './login.css'
import LoginForm from "../components/LoginForm/LoginForm.component";


const LoginPage = () => {
    return (
        <div className="login">
            <div className="loginForm">
            <LoginForm />
            </div>
        </div>
    )
}

export default LoginPage;