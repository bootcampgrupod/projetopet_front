import React from "react";
import './login.css'
import ForgotForm from "../components/ForgotForm/ForgotForm";


const ForgotPage = () => {
    return (
        <div className="login">
            <div className="loginForm">
            <ForgotForm />
            </div>
        </div>
    )
}

export default ForgotPage;