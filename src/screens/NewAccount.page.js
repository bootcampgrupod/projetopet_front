import React from "react";
import "./login.css";
import NewAccountForm from "../components/NewAccountForm/NewAccountForm";

const NewAccountPage = () => {
  return (
    <div className="login">
      <div className="loginForm">
        <NewAccountForm />
      </div>
    </div>
  );
};

export default NewAccountPage;
