import { Card, Avatar } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { UserOutlined } from "@ant-design/icons";

const { Meta } = Card;

const CardPet = () => {
  return (
    <Card
      style={{ width: 250, height: 70 }}
      cover={
        <img
          alt="example"
          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
        />
      }
      actions={[<Link to="/novopet"> Mais informações</Link>]}
    >
      <Meta
        avatar={<UserOutlined />}
        title="Card title"
        description="This is the description"
      />
    </Card>
  );
};

export default CardPet;
