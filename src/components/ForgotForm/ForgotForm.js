import { withRouter, useHistory } from "react-router-dom";
import { Form, Input, Button,  } from "antd";
import "./forgotform.css";
import { resetPassword } from "../../api/user";
import { success, error } from "../Message/Message.component";

const ForgotForm = (props) => {
  const history = useHistory();

  const onFinish = (values) => {
    resetPassword(values).then((res) => {
      success('O link para redefinição de senha foi enviado para seu e-mail!');
      history.push("/login");
    })
      .catch((err)=> {
        if(err.response.status === 404){
        error('E-mail não encontrado.');
        }else {
          error('Ocorreu um erro!')
        }
      })
  };

  return (
    <div className="container">
      <div className="container-form">
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <strong> Esqueceu sua senha? Relaxe! </strong>
          <h4>
            {" "}
            Enviaremos um link em seu e-mail para você redefinir sua senha!{" "}
          </h4>
          <div className="forgot-email">
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Por favor digite seu email",
                },
              ]}
            >
              <Input className="input_form" type="email" placeholder="Email" />
            </Form.Item>
          </div>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button button-login-form"
            >
              Enviar
            </Button>
          </Form.Item>
          <Form.Item>
            <Button
              type="secondary"
              onClick={props.history.goBack}
              className="button-back"
            >
              Voltar
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div id="line"></div>
      <div className="container-form-image">
        <img
          className="login-foto"
          alt="foto"
          src="http://s2.glbimg.com/QKbpa4NbXKz_qJYFJWiFcYXBM7c=/e.glbimg.com/og/ed/f/original/2015/07/23/caes-e-gatos.jpg"
        />
      </div>
    </div>
  );
};

export default withRouter(ForgotForm);
