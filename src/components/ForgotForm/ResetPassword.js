import { withRouter, useHistory } from "react-router-dom";
import { Form, Input, Button } from "antd";
import "./forgotform.css";
import { updatePassword } from "../../api/user";
import { success, error } from "../Message/Message.component";
import {useLocation} from "react-router-dom";

const ResetPassword = (props) => {
  const history = useHistory();
  const search = useLocation().search;
  const passwordToken = new URLSearchParams(search).get('token');
  const email = new URLSearchParams(search).get('email');

  
  const onFinish = (values)=> {
    let formData = new FormData();
    formData.append('email', email);
    formData.append('passwordToken', passwordToken);
    formData.append('password', values.password);
    formData.append('password_confirmation', values.password_confirmation)
    updatePassword(formData).then((res) =>{
      success('Sua senha foi redefinida com sucesso!')
      history.push('/login')
    })
    .catch((err) => {
      error('Ocorreu um erro.')
    })
  }
  return (
    <div className="container">
      <div className="container-form">
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <h2> Redefina sua senha!</h2>
          <div className="forgot-email">
            <Form.Item
              name="email"
            >
              <Input className="input_form" type="email" placeholder="Email" disabled defaultValue={email}/>
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Por favor digite sua nova senha",
                },
              ]}
            >
              <Input className="input_form" type="password" placeholder="* Nova senha" />
            </Form.Item>
            <Form.Item
              name="password_confirmation"
              dependencies={['password']}
              rules={[
                {
                  required: true,
                  message: "Por favor por favor confirme sua senha",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password_confirmation') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('A senha e confirmação de senha não são iguais!'));
                  },
                }),
              ]}
            >
              <Input className="input_form" type="password" placeholder="* Confirme sua nova senha" />
            </Form.Item>
          </div>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button button-login-form"
            >
              Redefinir senha
            </Button>
          </Form.Item>
          <Form.Item>
            <Button
              type="secondary"
              onClick={props.history.goBack}
              className="button-back"
            >
              Voltar
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div id="line"></div>
      <div className="container-form-image">
        <img
        className="login-foto"
          alt="foto"
          src="http://s2.glbimg.com/QKbpa4NbXKz_qJYFJWiFcYXBM7c=/e.glbimg.com/og/ed/f/original/2015/07/23/caes-e-gatos.jpg"
        />
      </div>
    </div>
  );
};

export default withRouter(ResetPassword);
