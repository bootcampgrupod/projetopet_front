import { Menu } from "antd";
import { SmileOutlined, SearchOutlined, HomeOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

const { SubMenu } = Menu;

const MenuComponent = () => {
  const handleClick = (e) => {
    console.log("click ", e);
  };

  return (
    <Menu
      onClick={handleClick}
      style={{ width: 256 }}
      defaultSelectedKeys={["1"]}
      defaultOpenKeys={["sub2", "sub3"]}
      mode="inline"
    >
      <SubMenu key="sub1" icon={<HomeOutlined />} title="Home">
        {/* <Menu.ItemGroup key="g1" title="Item 1">
            <Menu.Item key="1">Option 1</Menu.Item>
            <Menu.Item key="2">Option 2</Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup key="g2" title="Item 2">
            <Menu.Item key="3">Option 3</Menu.Item>
            <Menu.Item key="4">Option 4</Menu.Item>
          </Menu.ItemGroup> */}
      </SubMenu>
      <SubMenu key="sub2" icon={<SearchOutlined />} title="Pets Perdidos">
        <Menu.Item key="1">
          <Link to="/pets/perdidos"> Consultar</Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/novopet/perdido"> Cadastrar</Link>
        </Menu.Item>
        {/* <SubMenu key="sub3" title="Submenu">
            <Menu.Item key="7">Option 7</Menu.Item>
            <Menu.Item key="8">Option 8</Menu.Item>
          </SubMenu> */}
      </SubMenu>
      <SubMenu key="sub3" icon={<SmileOutlined />} title="Pets Encontrados">
        <Menu.Item key="3">
          <Link to="/pets/encontrados">Consultar</Link>
        </Menu.Item>
        <Menu.Item key="4">
          <Link to="/novopet/encontrado">Cadastrar</Link>
        </Menu.Item>
      </SubMenu>
    </Menu>
  );
};

export default MenuComponent;
