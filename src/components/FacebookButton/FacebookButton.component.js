import React from 'react';
import FacebookLogin from 'react-facebook-login';

const responseFacebook = (response) => {
  console.log(response);
}

const FacebookButton = () => {
    return (
        <FacebookLogin
          appId="422033555937685"
          autoLoad={true}
          fields="name,email,picture"
          scope="public_profile,user_friends"
          // onClick={componentClicked}
          callback={responseFacebook} />
    )
}
export default FacebookButton;
  