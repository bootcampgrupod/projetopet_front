import { withRouter, useHistory } from "react-router-dom";
import { Form, Input, Button, Switch } from "antd";
import foto from "../../assets/caes-e-gatos.jpg";
import "./newAccount.css";
import InputMask from "react-input-mask";
import { createUser } from "../../api/user";
import { success, error, warning } from "../Message/Message.component";

const NewAccountForm = (props) => {
  const history = useHistory();
  const onFinish = async (values) => {
    const user = {
      document: values.document.replace(/\D/g, ''),
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      telephone: values.telephone.replace(/\D/g, ''),
      password: values.password,
      password_confirmation: values.passwordConfirmation,
      address: values.address,
      ong: values.ong}
    createUser(user).then((res) => {
      success('Usuário criado com sucesso!')
      history.push('/login')
    }).catch((err) => {
      if(err.response.status === 422) {
        warning("Esse e-mail já está cadastrado!")
      }else {
      error('Ocorreu um erro, tente novamente!')
      }
    })
  };

  return (
    <div className="container">
      <div className="container-form-account">
        <h1 className="title"> Cadastre-se </h1>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <div>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Por favor digite seu email",
                },
              ]}
            >
              <Input
                className="input_form"
                type="email"
                placeholder="Digite seu Email*"
              />
            </Form.Item>
          </div>
          <div className="container-flex">
            <Form.Item
              name="firstName"
              rules={[
                {
                  required: true,
                  message: "Por favor digite seu nome",
                },
              ]}
            >
              <Input
                className="input_form item-left"
                type="text"
                placeholder="Primeiro Nome*"
              />
            </Form.Item>

            <Form.Item
              name="lastName"
              rules={[
                {
                  required: true,
                  message: "Por favor digite seu sobrenome",
                },
              ]}
            >
              <Input
                className="input_form half"
                type="text"
                placeholder="Sobrenome*"
              />
            </Form.Item>
          </div>
          <div className="container-flex">
            <Form.Item
              name="document"
              rules={[
                {
                  required: true,
                  message: "Por favor digite seu cpf",
                },
              ]}
            >
              <InputMask 
                mask="999.999.999-99" 
                maskChar="" 
                className="input_form" 
                placeholder="CPF" />
            </Form.Item>

            <Form.Item
              name="telephone"
              rules={[
                {
                  required: true,
                  message: "Por favor digite seu celular",
                },
              ]}
            >
              <InputMask 
                mask="( 99 ) 99999-9999" 
                className="input_form"
                placeholder="Celular"/>
            </Form.Item>
          </div>

          <Form.Item
            name="address"
            rules={[
              {
                required: true,
                message: "Por favor digite seu endereço",
              },
            ]}
          >
            <Input 
              className="input_form" 
              type="text" 
              placeholder="Endereço*" />
          </Form.Item>

          <div className="container-flex">
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Por favor digite seu senha",
                },
              ]}
            >
              <Input 
                className="input_form" 
                type="password" 
                placeholder="Senha*" 
              />
            </Form.Item>
            <Form.Item
              name="passwordConfirmation"
              rules={[
                {
                  required: true,
                  message: "Por favor confirme seu senha",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('Senha não confere!'));
                  },
                })
              ]}
            >
              <Input 
                className="input_form" 
                type="password" 
                placeholder="Confirme Senha*"
              />
            </Form.Item>
          </div>

          <div className="center">
            <p> Sou uma Ong: </p>
            <Form.Item
              name="ong"
              rules={[
                {
                  required: false
                },
              ]}
              valuePropName="checked"
            >
              <Switch />
            </Form.Item>
          </div>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button button-login-form"
            >
              Enviar
            </Button>
          </Form.Item>
          <Form.Item>
            <Button
              type="secondary"
              onClick={props.history.goBack}
              className="button-back"
            >
              Voltar
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div id="line"></div>
      <div className="container-form-image">
        <img src={foto} alt="foto" className="login-foto" />
      </div>
    </div>
  );
};

export default withRouter(NewAccountForm);
