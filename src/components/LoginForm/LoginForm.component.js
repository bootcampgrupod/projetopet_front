import { Form, Input, Button } from "antd";
import { decodeToken } from "react-jwt";
import { Link } from "react-router-dom";
import "./loginForm.css";
import foto from "../../assets/caes-e-gatos.jpg";

const LoginForm = () => {
  const onFinish = async (values) => {
    // const { REACT_APP_API_URL_USER } = process.env;
    // const loginUrl = `${url}/login`;
    const json = JSON.stringify({
      email: values.email,
      password: values.password,
    });

    await fetch("http://pethelp-user-api.azurewebsites.net/api/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: json,
    })
      .then((result) => result.json())
      .then((data) => {
        if (data.token) {
          const user = decodeToken(data.token);
          localStorage.setItem("token", data.token);
          localStorage.setItem("userId", user.user_id);
        }
        // window.location.href = "/home";
      });
  };

  return (
    <div className="container">
      <div className="container-form">
        <h1 className="title"> Login </h1>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Por favor digite seu email",
              },
            ]}
          >
            <Input className="input_form" type="email" placeholder="Email" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Por favor digite sua senha!",
              },
            ]}
          >
            <Input
              // prefix={<LockOutlined className="site-form-item-icon" />}
              className="input_form"
              type="password"
              placeholder="Senha"
            />
          </Form.Item>
          {/* {error && (
            <div class="ant-form-item-explain ant-form-item-explain-error">
              <div role="alert">Usuário ou senha inválidos</div>
            </div>
          )} */}

          <Form.Item>
            <Link to="/esqueciasenha" className="login-form-forgot right form">
              Esqueci minha senha
            </Link>
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button button-login-form"
            >
              Entrar
            </Button>
          </Form.Item>
          <Form.Item>
            <div className="form_new_account">
              <p className="form_text"> Não tem uma conta? </p>
              <Link className="login-form-forgot" to="/novaconta">
                Crie uma nova conta
              </Link>
            </div>
          </Form.Item>
        </Form>

        {/* <div
          className="fb-login-button facebook"
          data-width=""
          data-size="large"
          data-button-type="continue_with"
          data-layout="default"
          data-auto-logout-link="true"
          data-use-continue-as="false"
        ></div> */}
      </div>
      <div id="line"></div>
      <div className="container-form-image">
        <h2> Olá PetHelper!</h2>
        <div className="flex">
          <h3>Que bom ter você aqui </h3> <p className="heart"> ♥ </p>
        </div>

        <img src={foto} className="login-foto" alt="cao e gato" />
      </div>
    </div>
  );
};

export default LoginForm;
