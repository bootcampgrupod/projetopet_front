import { Link } from "react-router-dom";
import "./header.css";
import logo from "../../assets/pet-logo-header.png";

const Header = () => {
const isAuthenticated = localStorage.getItem('token') ? true : false;

  return (
    <header>
      <Link to="/">
        <img className="logo" src={logo} alt="logo" />
      </Link>
      {isAuthenticated && (
        <button
          className="logout"
          onClick={() => {
            localStorage.clear();
            window.location.href = "/";
          }}
        >
          {" "}
          Logout
        </button>
      )}
    </header>
  );
};

export default Header;
