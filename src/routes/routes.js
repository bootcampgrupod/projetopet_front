import ForgotPage from "../screens/Forgot.page";
import HomePage from "../screens/Home.page";
import LoginPage from "../screens/Login.page";
import NewAccountPage from "../screens/NewAccount.page";
import ListPets from "../screens/Pets/ListPets.page";
import NewPetPage from "../screens/Pets/NewPet.page";
import PetDetailsPage from "../screens/Pets/PetDetails.page";
import ResetPasswordPage from "../screens/ResetPassword.page";

const routes = [
  {
    component: HomePage,
    path: "/home",
    exact: true,
  },
  // {
  //   component: HomePage,
  //   path: "/",
  //   exact: true,
  // },
  {
    component: LoginPage,
    path: "/login",
    exact: true,
  },
  {
    component: ForgotPage,
    path: "/esqueciasenha",
    exact: true,
  },
  {
    component: NewAccountPage,
    path: "/novaconta",
    exact: true,
  },
  {
    component: ResetPasswordPage,
    path: "/novasenha",
    exact: true,
  },
  {
    component: NewPetPage,
    path: "/novopet/perdido",
    exact: true,
  },
  {
    component: ListPets,
    path: "/pets/encontrados",
    exact: true,
  },
  {
    component: NewPetPage,
    path: "/pets/perdidos",
    exact: true,
  },
  {
    component: NewPetPage,
    path: "/novopet/encontrado",
    exact: true,
  },
  {
    component: PetDetailsPage,
    path: "/pet/:id",
  },
  {
    component: HomePage,
    path: "/",
  },
];
export default routes;
