import React, { useEffect } from "react";
import { Route, useHistory, Redirect } from "react-router-dom";
// import { decodeToken } from "react-jwt";

// const PrivateRoute = ({ ...props }) => {
//   const history = useHistory();
//   const token = localStorage.getItem("token");
//   // const accessToken = localStorage.getItem("accessToken");

//   useEffect(() => {
//     if (window.location.pathname === "/") {
//       token ? history.push("/home") : history.push("/login");
//     }

//     if (token) history.push("/home");
//     if ((!token ) && window.location.pathname === "/home") history.push("/login");
//   }, [history, token]);

//   return <Route {...props} />;
// };

// export default PrivateRoute;

// import {
//   Route,
//   Redirect
// } from 'react-router-dom';

function PrivateRoute({ children, isAuthenticated, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;
