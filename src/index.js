import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch } from "react-router-dom";
import "./index.css";
import "antd/dist/antd.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import PrivateRoute from "./routes/PrivateRoute";
import LoginPage from "./screens/Login.page";
import ForgotPage from "./screens/Forgot.page";
import NewAccountPage from "./screens/NewAccount.page";
import ResetPasswordPage from "./screens/ResetPassword.page";
import ProtectedRoutes from "./routes/ProtectedRoute";
import PublicRoute from "./routes/PublicRoute";
import NewPetPage from "./screens/Pets/NewPet.page";
import HomePage from "./screens/Home.page";
import ListPets from "./screens/Pets/ListPets.page";
import PetDetailsPage from "./screens/Pets/PetDetails.page";
// import { InitFacebookSdk } from "./helpers/initFacebook";

// wait for facebook sdk before startup
// InitFacebookSdk().then(startApp);

const isAuthenticated = localStorage.getItem("token") ? true : false;

// function startApp() {
ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App>
        <Switch>
          <PublicRoute path="/login" isAuthenticated={isAuthenticated}>
            <LoginPage />
          </PublicRoute>
          <PublicRoute path="/novaconta" isAuthenticated={isAuthenticated}>
            <NewAccountPage />
          </PublicRoute>
          <PublicRoute path="/esqueciasenha" isAuthenticated={isAuthenticated}>
            <ForgotPage />
          </PublicRoute>
          <PublicRoute path="/novasenha" isAuthenticated={isAuthenticated}>
            <ResetPasswordPage />
          </PublicRoute>

          <PrivateRoute
            path="/novopet/encontrado"
            isAuthenticated={isAuthenticated}
          >
            <NewPetPage status="encontrado" statusId={2} />
          </PrivateRoute>
          <PrivateRoute
            path="/novopet/perdido"
            isAuthenticated={isAuthenticated}
          >
            <NewPetPage status="perdido" statusId={2} />
          </PrivateRoute>
          <PrivateRoute
            path="/pets/perdidos"
            exact
            isAuthenticated={isAuthenticated}
          >
            <ListPets status="Perdidos" statusId={1} />
          </PrivateRoute>
          <PrivateRoute
            path="/pets/encontrados"
            exact
            isAuthenticated={isAuthenticated}
          >
            <ListPets status="Encontrados" statusId={2} />
          </PrivateRoute>
          <PrivateRoute path="/pet/:id" isAuthenticated={isAuthenticated}>
            <PetDetailsPage />
          </PrivateRoute>
          <PrivateRoute path="/home" isAuthenticated={isAuthenticated}>
            <HomePage />
          </PrivateRoute>
          {/* <PrivateRoute
            path="/"
            isAuthenticated={isAuthenticated}
          >
            <ProtectedRoutes />
          </PrivateRoute> */}
          {/* <Route path="*">
            <NoFoundComponent />
          </Route> */}
        </Switch>
      </App>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
// }

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
