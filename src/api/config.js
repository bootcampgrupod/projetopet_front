import axios from "axios";

const config = {
  // baseURL:
  //   "https://cors-anywhere.herokuapp.com/https://pethep-users-api.herokuapp.com/api",
  // // timeout: 1000,
  headers: { 
  "Access-Control-Allow-Origin": "*, https://pethep-users-api.herokuapp.com, http://localhost:3000",
  "Access-Control-Allow-Methods": "*, POST, GET, OPTIONS",
  "Content-Type": 'application/json;charset=utf-8',
  'Accept': 'application/json',
  },
};
// 

// axios.defaults.baseURL = 'http://Dominio';
// axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
// axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

// const usuario = JSON.parse(localStorage.getItem("usuario"));

// if (usuario) {
//   config.headers = {
//     Authorization: usuario.token,
//   };
// }
const protocol = axios.create(config);

export default protocol;
