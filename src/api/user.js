import protocol from "./config";

const { REACT_APP_API_URL_USER } = process.env;

const url = REACT_APP_API_URL_USER

export function login(user) {
  const loginUrl = `${url}/login`;
  const json = JSON.stringify({
    email: user.email,
    password: user.password,
  });

  return protocol.post(loginUrl, json
  );
}

export function createUser(user) {
  const resetUrl = `${url}/register`;

  return protocol.post(resetUrl, JSON.stringify(user));
}

export function resetPassword(value) {
  const resetUrl = `${url}/req-password-reset`;
  const formData = new FormData()
    formData.append('email', value.email)
  return protocol.post(resetUrl, formData);
}

export const updatePassword = (values) => {
  const updateUrl = `${url}/update-password`;
 
  return protocol.post(updateUrl, values);
}