import protocol from "./config";

const { REACT_APP_API_URL_PETS } = process.env;

const url = REACT_APP_API_URL_PETS;

export function getPetStatus() {
  const statusUrl = `${url}/posts/status`;
  return protocol.get(statusUrl);
}

export function getPetStatusById(statusId, page) {
  const statusUrl = `${url}/posts/status/${statusId}?limit=8&page=${page}`;

  return protocol.get(statusUrl);
}

export function getPetById(petId) {
  const petsUrl = `${url}/posts/${petId}`;

  return protocol.get(petsUrl);
}

export function postPet(obj) {
  const petUrl = `${url}/posts`;
  return protocol.post(petUrl, JSON.stringify(obj));
}

export function getPetByStatusId(statusId) {
  const petsUrl = `${url}/posts/status/${statusId}`;

  return protocol.get(petsUrl);
}
