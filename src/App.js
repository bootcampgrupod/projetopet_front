import { withRouter } from "react-router-dom";
import Header from "./components/Header/Header.component";
import MenuComponent from "./components/Menu/Menu.component";
import './App.css'

function App(props) {
  // if (localStorage.getItem("token")) {
  //   window.location.href = "/home";
  // }


const isAuthenticated = localStorage.getItem('token') ? true : false;

  return (
    <div className="App">
      <Header />
      <div id="main">
       {isAuthenticated && <MenuComponent />}
        <main>
          <div className="content" id="content">
            {props.children}
          </div>
        </main>
      </div>
    </div>
  );
}

export default withRouter(App);
